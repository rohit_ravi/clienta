import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmployeeService } from '../service/employee.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
  selector: 'login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  result:any;
  registerForm: FormGroup;
  submitted = false;
  isLoggedIn:any;
  constructor(private formBuilder: FormBuilder, private toastr:ToastrService,
    private service:EmployeeService,
    private router:Router) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
        email: [null, [Validators.required]],
        password: [null, [Validators.required]]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }


  /**Login button */
  fnLogin() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      this.toastr.error("Login","Login fields must be filled.",{
        timeOut:2000
      });
      return;
    }
    this.postData();
  }

  /** It is used for send the data from client to server */
  postData(){
    this.isLoggedIn=sessionStorage.getItem("isLogged");
    if(this.isLoggedIn){
      this.router.navigate(['/dashboard']);
    }
    this.service.login(this.registerForm.value).subscribe((response)=>{
      if(response.login=="success"){
        this.toastr.success("Login","Login successful");
        sessionStorage.isLogged=true;
        this.router.navigate(['/dashboard']);
      }
      else{
        this.toastr.error("Error","Email or Password is wrong!!");
      }
    },(err:HttpErrorResponse)=>{
      console.log("Error occured");
    });
  }

  /** It is used for navigate from login component to Register component */
  Register(){
    this.toastr.info("Registation","Switched to registration component",{
      timeOut:2000
    });
    this.router.navigate(["/registration"]);
  }
}
