import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import { EmployeeService } from '../service/employee.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styles: []
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;

    constructor(private formBuilder: FormBuilder, private toastr:ToastrService,
              private service:EmployeeService,
              private router:Router) { }

    ngOnInit() {
      this.registerForm = this.formBuilder.group({
        firstname: [null, Validators.required],
        lastname: [null, Validators.required],
        email: [null, [Validators.required, Validators.email]],
        password: [null, [Validators.required, Validators.minLength(6)]]
      });
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;
        
        if (this.registerForm.invalid) {
            this.toastr.error("Registration","Registration filed must be filled",{
              timeOut:2000
            });
            return;
        }
        this.postData();
        this.toastr.success("Registration","Registration Successfull",{
          timeOut:2000
        });
        this.router.navigate(["/login"]);
    }
    postData(){
      this.service.register(this.registerForm.value).subscribe();
    }

    /** It is used for to create register component  to login component */
    Login(){
      this.toastr.info("Login","Switched to Login Component",{
        timeOut:2000
      });
      this.router.navigate(["/login"]);
    }
}
