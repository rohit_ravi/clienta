import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './component/register.component';
import { LoginComponent } from './component/login.component';
import { HomeComponent } from './component/home.component';
const routes: Routes = [
  {
    path:"home",component:HomeComponent
  },
  {
    path:"registration",component:RegisterComponent
  },
  {
    path:"login",component:LoginComponent
  },
  {
    path:"dashboard",loadChildren:"./module/dashboard.module#DashboardModule"
  },
  {
    path:"**",redirectTo:"home",pathMatch:"full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
