import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './component/dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { 
  MatToolbarModule,
  MatIconModule,
  MatButtonModule
} from "@angular/material";
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './component/about.component';
import { CountryComponent } from './component/country.component';

/**Routing Path */
export const appRoutes:Routes=[
    {
      path:"",component:DashboardComponent,
      children:[
        {
          path:"about",component:AboutComponent
        },
        {
          path:"country",component:CountryComponent
        }
      ]
    }
]

@NgModule({
  declarations: [
    DashboardComponent,  
    AboutComponent, 
    CountryComponent
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    RouterModule.forChild(appRoutes),
    HttpClientModule
  ],
  exports:[DashboardComponent]
})
export class DashboardModule { }
