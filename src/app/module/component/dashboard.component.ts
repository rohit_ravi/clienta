import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styles: [
    `
    footer{
      background-color:#3f51b5;
      text-align: center;
      padding:10px;
      color:white;
      width:100%;
      position:fixed;
      bottom:0px;
  }
  a:hover{
    color:white;
    text-decoration:none;
  }
    `
  ]
})
export class DashboardComponent implements OnInit {
  private isLoggedIn:any;
  constructor(private router:Router,private toastr:ToastrService) { }
  ngOnInit() {
  }
  logout(){
    this.isLoggedIn=sessionStorage.getItem("isLogged");
    if(!this.isLoggedIn){
      this.router.navigate(['/login']);
      sessionStorage.clear();
    }
    this.router.navigate(['/login']);
    this.toastr.success("Logout","Logout Successfully");
    sessionStorage.clear();

  }
  
}
