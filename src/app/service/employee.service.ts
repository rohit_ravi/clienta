import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import URLS from '../URLS/urls';

const headerOptions={
  headers:new HttpHeaders({'content-type':'application/json'})
}

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http:HttpClient) { }

  register(obj:any):Observable<any>{
    return this.http.post(URLS+"/emp-register",obj,headerOptions);
  }

  login(obj:any):Observable<any>{
    return this.http.post(URLS+"/login",obj,headerOptions);
  }
}
